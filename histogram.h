file *hist_equal(file *p)
{
	int **matrix = p->matrix;
	int **out;
	int row=p->row,column=p->column,byte=p->byte,i,j,sum=0,size;
	int *hist = (int *)calloc(256,sizeof(int));
	int *sum_hist = (int *)calloc(256,sizeof(int));
	size = row*column;
	out = (int **)malloc(row*sizeof(int *));
	if(!hist)
	{
		printf("\nno more space\n");
		exit(1);
	}
	for(i=0;i<row;i++)
		for(j=0;j<column;j++)
			hist[matrix[i][j]]++;
	for(i=0;i<=255;i++)
	{
		sum = sum+hist[i];
		sum_hist[i] = sum;
	}
	double d =(double)byte/size;
	for(i=0;i<row;i++)
	{
		out[i]=(int *)malloc(column*sizeof(int));
		for(j=0;j<column;j++)
			out[i][j] = d*sum_hist[matrix[i][j]];
	}
	p->matrix=out;
	free(matrix);
	return p;
}

file *mean_filter(file *p)
{
	int row = p->row;
	int column = p->column;
	int i,j,k,l,x;
	int **a = p->matrix;
	int **b = (int **)calloc(row,sizeof(int *));
	for(i=0;i<row;i++)
		b[i]=(int *)calloc(column,sizeof(int));
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
		{
			x=0;
			for(k=i-size/2;k<=i+size/2;k++)
			{
				for(l=j-size/2;l<=j+size/2;l++)
				{
					if((k<row && l<column) && (k>=0 && l>=0)) //image filtering by taking average of pixel values
						x+=a[k][l];			  //sorrounding a pixel
					else
						x+=a[i][j];
				}
			}
			b[i][j]=x/(size*size);
		}
	}
	p->matrix=b;
	free(a);
	return p;
}

file *median_filter(file *p)
{
	int row = p->row;
	int column = p->column;
	int **a = p->matrix;
	int **b = (int **)malloc(row*sizeof(int *));
	int i,j,k,l,x;
	int c[size*size];
	for(i=0;i<row;i++)
	{
		b[i] = (int *)malloc(column*sizeof(int));
		for(j=0;j<column;j++)
		{
			x=0;
			for(k=(i-size/2);k<=(i+size/2);k++)
			{
				for(l=(j-size/2);l<=(j+size/2);l++)
				{
					if((k<row && l<column) && (k>=0 && l>=0)) //image filtering by taking median of pixel values
						c[x++]=a[k][l];			  //sorrounding a pixel
					else

						c[x++]=a[i][j];
				}
			}
			qsort(c,size*size,sizeof(int),cmpfunc);
			if((size*size)%2)
				x=(size*size+1)/2;
			else
				x=(((size*size)/2)+((size*size)/2+1))/2;
			b[i][j]=c[x];
		}
	}
	p->matrix=b;
	free(a);
	return p;
}

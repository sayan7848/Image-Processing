#include<stdio.h>
#include<stdlib.h>
#include<math.h>

struct info
{
	int **matrix;
	int row,column,byte;
	char c[3];
};
typedef struct info file;

int size;

file *read(FILE *fp)
{
	int i,j,byte,row,column;
	int in=0;
	char c,d[50];
	i=0;
	printf("\nenter input file name\n");
	scanf("%s",d);
	fp=fopen(d,"r");
	int **a;
	file *p = (file *)malloc(sizeof(file));
	while((c=getc(fp))!='\n')	//image header
		p->c[in++]=c;
	p->c[in]='\0';
	if(getc(fp)=='#')
		while(getc(fp)!='\n'); //checking for comment
	else
		fseek(fp,-1,SEEK_CUR); //if comment unavailable pointer moves to beginning of the line
	fscanf(fp,"%d%d",&row,&column); //reads row and column info
	fscanf(fp,"%d",&byte); //reads byte info
	a=(int **)malloc(row*sizeof(int *));
	for(i=0;i<row;i++)
	{
		a[i]=(int *)malloc(column*sizeof(int));
		for(j=0;j<column;j++)
			fscanf(fp,"%d",&a[i][j]); //stores pixel values as a matrix
		getc(fp);
	}
	p->matrix = a;               //storing values in a structure
	p->row = row;
	p->column = column;
	p->byte = byte;
	fclose(fp);
	return p;
}

void write(file *p)
{
	int row = p->row;
	int column = p->column;
	int **a = p->matrix;
	FILE *fp;
	fp=fopen("output_nip.pnm","w");
	int i,j;
	fputs(p->c,fp);
	fprintf(fp,"\n");
	fprintf(fp,"%d %d\n",row,column);
	fprintf(fp,"%d\n",p->byte);
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
			fprintf(fp,"%d\n",a[i][j]);     //writing output file
	}
	fclose(fp);
	free(p);
}
file *binarization(file *p)
{
	int i,j,sum,k,l;
	float mean,sd,t;
	for(i=1;i<(p->row-1);i++)
	{
		for(j=1;j<(p->column-1);j++)
		{
			mean=0;
				sum=0;
				for(k=i-1;k<=i+1;k++)
				{
					for(l=j-1;l<=j+1;l++)
					{
						mean=mean+(p->matrix)[k][l];
						sum=sum +(p->matrix)[k][l]*(p->matrix)[k][l];
					}
				}
				mean=mean/9;
				sd=sqrt((sum/9)-(mean)*(mean));
				t=mean -0.2*sd;
				if((p->matrix)[i][j]<t)
					(p->matrix)[i][j]=0;
				else
					(p->matrix)[i][j]=255;
				
		}
	}
	return p;
}
			
					

int main()
{
	FILE *fp;
	file *q;
	q = read(fp);
	q = binarization(q);
	 q = binarization(q);	
	write(q);
	return 0;
}

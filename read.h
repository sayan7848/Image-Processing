file *read(FILE *fp)
{
	int i,j,byte,row,column;
	int in=0;
	char c,d[50];
	i=0;
	printf("\nenter input file name\n");
	scanf("%s",d);
	fp=fopen(d,"r");
	int **a;
	file *p = (file *)malloc(sizeof(file));
	while((c=getc(fp))!='\n')	//image header
		p->c[in++]=c;
	p->c[in]='\0';
	if(getc(fp)=='#')
		while(getc(fp)!='\n'); //checking for comment
	else
		fseek(fp,-1,SEEK_CUR); //if comment unavailable pointer moves to beginning of the line
	fscanf(fp,"%d%d",&row,&column); //reads row and column info
	fscanf(fp,"%d",&byte); //reads byte info
	a=(int **)malloc(row*sizeof(int *));
	for(i=0;i<row;i++)
	{
		a[i]=(int *)malloc(column*sizeof(int));
		for(j=0;j<column;j++)
			fscanf(fp,"%d",&a[i][j]); //stores pixel values as a matrix
		getc(fp);
	}
	p->matrix = a;               //storing values in a structure
	p->row = row;
	p->column = column;
	p->byte = byte;
	fclose(fp);
	return p;
}
